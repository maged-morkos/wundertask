//
//  ListViewController.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/16/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    var placemarksArray : [Placemark] = []
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OpenMap"{
            let indexPath = sender as? IndexPath
            let mapView = segue.destination as? MapViewController
            let placemarkArray = [self.placemarksArray[(indexPath?.row)!]]
            mapView?.placemarksArray = placemarkArray
        }
    }
}

extension ListViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placemarksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PlacemarkTableViewCell = tableView.dequeueReusableCell(withIdentifier: "placemarkCell", for: indexPath) as! PlacemarkTableViewCell
        cell.fillCell(placemark: self.placemarksArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "OpenMap", sender: indexPath)
    }
    
}

