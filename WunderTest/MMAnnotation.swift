//
//  MMAnnotation.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/16/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//


import MapKit

class MMAnnotation: NSObject, MKAnnotation {

    let locationName: String
    let coordinate: CLLocationCoordinate2D
    
    init(placemark : Placemark) {
        self.locationName = placemark.name!
        
        self.coordinate = CLLocationCoordinate2D(latitude: placemark.coordinates![1], longitude: placemark.coordinates![0])
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
    
}
