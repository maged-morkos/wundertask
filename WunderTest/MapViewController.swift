//
//  MapViewController.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/16/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    var placemarksArray : [Placemark] = []
    var annotations: [MMAnnotation] = []
    let regionRadius: CLLocationDistance = 1000
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        self.checkLocationAuthorizationStatus()
        addAnotationsToMap()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addAnotationsToMap() {
        if placemarksArray.count > 0{
            let initialLocation = CLLocation(latitude: placemarksArray[0].coordinates![1], longitude: placemarksArray[0].coordinates![0])
            centerMapOnLocation(location: initialLocation)
            for placemark : Placemark in placemarksArray {
                annotations.append(MMAnnotation(placemark: placemark))
            }
            mapView.addAnnotations(annotations)
        }
        
    }
    
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? MMAnnotation else { return nil }
        
        let identifier = "car"
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        let annotation = view.annotation as? MMAnnotation
        self.centerMapOnLocation(location: CLLocation(latitude: (annotation?.coordinate.latitude)!, longitude: (annotation?.coordinate.longitude)!))
        let annotationsArray = mapView.annotations
        for ann in annotationsArray {
            if !(annotation?.isEqual(ann))!{
                mapView.view(for: ann)?.isHidden = true
            }
        }
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        let annotationsArray = mapView.annotations
        for ann in annotationsArray {
            mapView.view(for: ann)?.isHidden = false
        }
    }
}
