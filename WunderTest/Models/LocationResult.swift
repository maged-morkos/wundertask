//
//  LocationResult.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/15/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//

import Foundation

struct LocationResult: Decodable {
    let placemarks: [Placemark]
}
