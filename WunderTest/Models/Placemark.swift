//
//  Placemark.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/15/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//

import Foundation

struct Placemark: Decodable {
    
    let address: String?
    let engineType: String?
    let exterior: String?
    let fuel: Int?
    let interior: String?
    let name: String?
    let vin: String?
    let coordinates: [Double]?
}
