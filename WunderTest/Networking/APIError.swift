//
//  APIError.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/15/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//


// This for converting api errors to redable errors and it also can be localized depend on business need
import Foundation

enum APIError: Error {
    case requestFailed
    case jsonConversionFailure
    case invalidData
    case responseUnsuccessful
    case jsonParsingFailure
    var localizedDescription: String {
        switch self {
        case .requestFailed: return "Request Failed"
        case .invalidData: return "Invalid Data"
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonParsingFailure: return "JSON Parsing Failure"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        }
    }
}
