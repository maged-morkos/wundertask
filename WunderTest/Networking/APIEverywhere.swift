//
//  APIEverywhere.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/15/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//

// using APIClient to fetch the data with passing the request and the model, all calls suppose to be here
import Foundation

class APIEverywhere: APIClient {
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    func getPlacemarks(from urlType: ApiUrls, completion: @escaping (Result<LocationResult?, APIError>) -> Void) {
        fetch(with: urlType.request , decode: { json -> LocationResult? in
            guard let locationResult = json as? LocationResult else { return  nil }
            return locationResult
        }, completion: completion)
    }
}
