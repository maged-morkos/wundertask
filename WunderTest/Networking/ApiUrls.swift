//
//  ApiUrls.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/15/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//

import Foundation
enum ApiUrls {
    case locations
}
extension ApiUrls: Endpoint {
    
    var base: String {
        return "https://s3-us-west-2.amazonaws.com"
    }
    
    var path: String {
        switch self {
            // add more cases if there are more urls available
        case .locations: return "/wunderbucket/locations.json"
        }
    }
}
