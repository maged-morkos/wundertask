//
//  PlacemarkTableViewCell.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/16/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//

import UIKit

class PlacemarkTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var engineTypeLabel: UILabel!
    @IBOutlet weak var exteriorLabel: UILabel!
    @IBOutlet weak var fuelLabel: UILabel!
    @IBOutlet weak var interiorLabel: UILabel!
    @IBOutlet weak var vinLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillCell(placemark : Placemark) {
        self.nameLabel.text = placemark.name
        self.addressLabel.text = "Address : \(String(describing: placemark.address!))"
        self.engineTypeLabel.text = "Engine Type: \(String(describing: placemark.engineType!))"
        self.exteriorLabel.text = "Exterior: \(String(describing: placemark.exterior!))"
        self.fuelLabel.text = "Fuel: \(String(describing: placemark.fuel!))"
        self.interiorLabel.text = "Interior: \(String(describing: placemark.interior!))"
        self.vinLabel.text = "Vin: \(String(describing: placemark.vin!))"
    }
}
