//
//  ViewController.swift
//  WunderTest
//
//  Created by Maged Morkos on 8/15/18.
//  Copyright © 2018 Maged Morkos. All rights reserved.
//

import UIKit
import SVProgressHUD
class ViewController: UIViewController {
    
    private let client = APIEverywhere()
    private var placemarksArray : [Placemark] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        SVProgressHUD.setBackgroundColor(#colorLiteral(red: 0.6149347425, green: 0.7828714848, blue: 0.2782411873, alpha: 1))
        
        DispatchQueue.global(qos: .background).async {
            self.getPlacemarks()
        }
    }
    
    func getPlacemarks() {
        client.getPlacemarks(from: .locations) { result in
            
            switch result {
            case .success(let locationResult):
                guard let placemarksArray = locationResult?.placemarks else { return }
                self.placemarksArray = placemarksArray
            case .failure(let error):
                print("the error \(error)")
            }
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "List"{
            let listViewController : ListViewController = segue.destination as! ListViewController
            listViewController.placemarksArray = self.placemarksArray
        }else{
            let mapViewController : MapViewController = segue.destination as! MapViewController
            mapViewController.placemarksArray = self.placemarksArray
        }
    }

}

